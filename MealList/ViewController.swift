//
//  ViewController.swift
//  MealList
//
//  Created by  drake on 2020/05/31.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var mealTableView: UITableView!
    
    var mealList: [MealModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let loadedMeals = loadMeals() {
            self.mealList = loadedMeals
        }
        
        if mealList.count == 0 {
            let dummy1 = MealModel.init(name: "스파게티", photo: UIImage(named: "meal1"), rating: 3)
            let dummy2 = MealModel.init(name: "케찹", photo: UIImage(named: "meal2"), rating: 5)
            let dummy3 = MealModel.init(name: "파스타", photo: UIImage(named: "meal3"), rating: 2)
            mealList.append(dummy1)
            mealList.append(dummy2)
            mealList.append(dummy3)
        }
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentDetail" {
            
        } else if segue.identifier == "showDetail" {
            let detailVC = segue.destination as! DetailViewController
            let selectedCell = sender as! MealCell
            if let selectedIndexPath = mealTableView.indexPath(for: selectedCell) {
                detailVC.mealModel = mealList[selectedIndexPath.row]
            }
            
        }
    }
    
    var isEditMode = false
    
    @IBAction func doEdit(_ sender: Any) {
        isEditMode = !isEditMode
        mealTableView.setEditing(isEditMode, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            mealList.remove(at: indexPath.row)
            mealTableView.deleteRows(at: [indexPath], with: .automatic)
            saveMeals()
        }
    }
    
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        guard let detailVC = sender.source as? DetailViewController else {
            return
        }
        
        if let selectedRow = self.mealTableView.indexPathForSelectedRow {
            mealList[selectedRow.row] = detailVC.mealModel
            self.mealTableView.reloadRows(at: [selectedRow], with: .none)
        } else {
            
            let insertIndexPath = IndexPath(row: mealList.count, section: 0)
            mealList.append(detailVC.mealModel)
            self.mealTableView.insertRows(at: [insertIndexPath], with: .automatic)
        }
        saveMeals()
        
    }
    
    func saveMeals() {
        //archive
        
        //path
        DispatchQueue.global().async {
            let documentDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first
            guard let archiveURL = documentDirectory?.appendingPathComponent("meals") else {
                return
            }
            
            do {
                let archvedData =  try NSKeyedArchiver.archivedData(withRootObject: self.mealList, requiringSecureCoding: true)
                try archvedData.write(to: archiveURL)
                print(archiveURL.absoluteString)
            } catch {
                print(error)
            }
        }
    }
    
    func loadMeals() -> [MealModel]? {
        let documentDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first
        guard let archiveURL = documentDirectory?.appendingPathComponent("meals") else {
            return nil
        }
        
        guard let codedData = try? Data(contentsOf: archiveURL) else {
            return nil
        }
        
        guard let unarchivedData = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) else {
            return nil
        }
        
        return unarchivedData as? [MealModel]
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mealList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mealCell = tableView.dequeueReusableCell(withIdentifier: "mealCell", for: indexPath) as! MealCell
        mealCell.name.text = mealList[indexPath.row].name
        mealCell.ratingView.rating = mealList[indexPath.row].rating
        mealCell.mealImageView.image = mealList[indexPath.row].photo ?? UIImage(named: "defaultPhoto")
        
        return mealCell
    }
}
