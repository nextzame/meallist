//
//  DetailViewController.swift
//  MealList
//
//  Created by  drake on 2020/05/31.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

extension DetailViewController: RatingViewDelegate {
    func ratingStatusChanged() {
        saveButtonStatus()
    }
}

class DetailViewController: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var mealImageView: UIImageView!
    @IBOutlet weak var ratingView: RatingView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var mealModel = MealModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ratingView.delegate = self

        nameField.text = mealModel.name
        mealImageView.image = mealModel.photo ?? UIImage(named: "defaultPhoto")
        ratingView.rating = mealModel.rating
        
        saveButton.isEnabled = false
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(imageTap))
        mealImageView.addGestureRecognizer(tapGesture)
        mealImageView.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    @objc func imageTap() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func closeVC(_ sender: Any) {
        let presentVC = presentingViewController is UINavigationController
        
        if presentVC == true {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func didChanged(_ sender: UITextField) {
        saveButtonStatus()
        
    }
    
    func saveButtonStatus() {
        if nameField.text?.isEmpty ?? true {
            saveButton.isEnabled = false
        } else {
            saveButton.isEnabled = true
        }
    }
    
    @IBAction func saveMeal(_ sender: Any) {
        mealModel.photo = mealImageView.image
        mealModel.rating = ratingView.rating
        mealModel.name = nameField.text ?? ""
        
        self.performSegue(withIdentifier: "toMealList" , sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController : UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            return
        }
        
        mealImageView.image = selectedImage
        
        saveButtonStatus()
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailViewController : UINavigationControllerDelegate {
    
}
